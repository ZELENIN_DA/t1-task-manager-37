package ru.t1.dzelenin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.repository.IUserRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.IPropertyService;
import ru.t1.dzelenin.tm.api.service.IUserService;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.exception.entity.EntityNotFoundException;
import ru.t1.dzelenin.tm.exception.entity.UserNotFoundException;
import ru.t1.dzelenin.tm.exception.field.EmailEmptyException;
import ru.t1.dzelenin.tm.exception.field.LoginEmptyException;
import ru.t1.dzelenin.tm.exception.field.PasswordEmptyException;
import ru.t1.dzelenin.tm.exception.user.ExistsEmailException;
import ru.t1.dzelenin.tm.exception.user.ExistsLoginException;
import ru.t1.dzelenin.tm.model.User;
import ru.t1.dzelenin.tm.repository.UserRepository;
import ru.t1.dzelenin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Optional;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IPropertyService propertyService;

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }


    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(Role.USUAL);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isMailExist(email)) throw new ExistsEmailException();
        @NotNull final Connection connection = getConnection();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(Role.USUAL);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            if (role == null) user.setRole(Role.USUAL);
            else user.setRole(role);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email);
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        return removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @NotNull final User user = Optional.ofNullable(findByEmail(email))
                .orElseThrow(UserNotFoundException::new);
        return removeOne(user);
    }

    @Override
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @Override
    public User updateUser(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Nullable
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @Nullable
    @Override
    public Boolean isMailExist(@NotNull final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

}
