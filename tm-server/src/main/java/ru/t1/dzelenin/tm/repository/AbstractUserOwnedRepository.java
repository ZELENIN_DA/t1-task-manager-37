package ru.t1.dzelenin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dzelenin.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model);

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? ORDER BY %s", getTableName(), getSortType(comparator)
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND user_id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String userId, @NotNull final String id) {
        final M model = findOneById(id, userId);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@NotNull final String userId, @NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? AND user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public long getCount(@NotNull final String userId) {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getLong("count");
            }
        }
    }

}


