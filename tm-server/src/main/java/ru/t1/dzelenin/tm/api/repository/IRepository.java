package ru.t1.dzelenin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    M update(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @NotNull
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    long getCount();

    boolean existsById(@NotNull String id);

}